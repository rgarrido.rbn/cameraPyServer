#!/bin/python3

#  main.py
#  Date: Apr 17 07:49:03
#  Author: Rubén Garrido 
#  Mail: rgarrido.rbn@gmail.com

import base64
from flask import Flask, request

app = Flask(__name__)

@app.route('/photo', methods=['POST'])
def photo():
    content_type = request.headers.get('Content-Type')
    if content_type == 'image/jpeg':
        #img_data = request.get_data() ## without base64
        img_data = request.get_data()
        with open('photo.jpg', 'wb') as f:
            f.write(img_data)
        return 'Imagen recibida y almacenada', 200
    else:
        return 'Se esperaba una imagen JPEG', 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)


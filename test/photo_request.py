#!/bin/python3

#  photo_request.py
#  Date: Apr 17 12:06:39
#  Author: Rubén Garrido 
#  Mail: rgarrido.rbn@gmail.com

import requests

# Archivo de imagen para enviar
file_path = 'sample.jpg'

# URL del servidor para hacer la petici\u00f3n POST
url = 'http://localhost:3000/photo'

# Abrir y leer el archivo de imagen
with open(file_path, 'rb') as f:
    img_data = f.read()

# Hacer la petici\u00f3n POST con los datos de la imagen
response = requests.post(url, data=img_data, headers={'Content-Type': 'image/jpeg'})

# Imprimir la respuesta del servidor
print(response.text)
